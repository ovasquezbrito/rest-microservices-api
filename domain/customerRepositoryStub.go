package domain

type CustomerRepositoryStub struct {
	customers []Customer
}

func (s CustomerRepositoryStub) FindAll() ([]Customer, error) {
	return s.customers, nil
}

func NewCustomerRepositoryStub() CustomerRepositoryStub {
	customers := []Customer{
		{Id: "1001", Name: "Oduber", City: "Maturin", Zipcode: "6201", DateofBirth: "1974-04-13", Status: "1"},
		{Id: "1002", Name: "Karelyan", City: "Caracas", Zipcode: "6201", DateofBirth: "1980-04-13", Status: "1"},
	}

	return CustomerRepositoryStub{customers: customers}
}
