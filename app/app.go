package app

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/ovasquezbrito/restmicro/domain"
	"github.com/ovasquezbrito/restmicro/service"
)

func Start() {

	r := mux.NewRouter()

	ch := CustomerHandlers{service: service.NewCustomerService(domain.NewCustomerRepositoryStub())}
	r.HandleFunc("customers", ch.getAllCustomers).Methods(http.MethodGet)

	log.Fatal(http.ListenAndServe("localhost:8000", r))

}
